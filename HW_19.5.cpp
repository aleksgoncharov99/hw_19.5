#include <iostream>
#include <string>

class Animal
{ 
private: 

public:
	virtual void Voice()
	{
		std::cout << " ";
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Meow ";
	}

};

class Dog : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Woof ";
	}

};

class Fox : public Animal
{
public:
	void Voice() override
	{
		std::cout << "??? ";
	}

};



int main()
{
	Animal* mas[10];
	// ��������� ������ ���������� ���������� ���������.
	for (int i = 0; i < 10; i++)
	{
		int x = rand() % 3 + 1;
		switch (x)
		{
		case 1: mas[i] = new Cat();
			break;
		case 2: mas[i] = new Dog();
			break;
		case 3: mas[i] = new Fox();
			break;
		
		}
	}
	// ���������� �� �������
	for (Animal* a : mas)
	{
		a->Voice();
	}

	/*Cat* p = new Cat;
	Dog* p1 = new Dog;
	Fox* p2 = new Fox;
	Animal* t = new Animal;
	p->Voice();
	p1->Voice();
	p2->Voice();
	t->Voice();
	*/

}
